<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Poll extends Model
{
    use HasFactory;

    //protected $guarded = ['*'];
    protected $fillable = [
        'question_text','choice_1','choice_2','choice_3','count1','count2','count3'
    ];
}
