<?php

use App\Models\Poll;

Route::get('/', function() {
    return view('home');
});

Route::post('/polls', function() {
    $poll = new Poll;

    $poll->question_text = request('question_text');
    $poll->choice_1 = request('question_choice_1');
    $poll->choice_2 = request('question_choice_2');
    $poll->choice_3 = request('question_choice_3');

    $poll->save();

    dd('Sondage créé avec succès!');
});

Route::get('/polls/{id}', function($id) {
    $poll = Poll::findOrFail($id);

    return view('polls.show', compact('poll'));
});

Route::post('/polls/{id}/vote', function($id) {
    $poll = Poll::findOrFail($id);
    $poll_chosen = request('choice');
    //return $poll_chosen;
    if($poll_chosen == 'choice_1'){
        $poll_name  = $poll->choice_1;
        $old_count1 = $poll->count1;
        $poll->update([
                       'count1' => $old_count1+1
                       ]);
        $count_poll = $poll->count1;
        
        return view('polls.countvote',compact('poll','poll_chosen','count_poll','poll_name'));

    }
    if($poll_chosen == 'choice_2'){
        $poll_name  = $poll->choice_2;

        $old_count2 = $poll->count2;
        $poll->update([
            'count2' => $old_count2+1
            ]);
        $count_poll = $poll->count2;

        return view('polls.countvote',compact('poll','poll_chosen','count_poll','poll_name'));
    }
    if($poll_chosen == 'choice_3'){
        $poll_name  = $poll->choice_3;

        $old_count3 = $poll->count3;
        $poll->update([
            'count3' => $old_count3+1
            ]);
        $count_poll = $poll->count3;

        return view('polls.countvote',compact('poll','poll_chosen','count_poll','poll_name'));
    }
    /*$count_old_number = $poll->count_number_chosen;
    
    return $count_old_number;*/

   /* $poll->update([
        'count_number_chosen'  => 
    ]);
    */
    //dd(request('choice'));
});
